FROM node

RUN mkdir /skillbox

WORKDIR /skillbox

COPY . .

RUN yarn install
RUN yarn test
RUN yarn build

CMD yarn start

